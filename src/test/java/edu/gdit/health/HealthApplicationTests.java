package edu.gdit.health;

import edu.gdit.health.dao.SysReportDao;
import edu.gdit.health.model.SysReport;
import edu.gdit.health.spring.Human;
import edu.gdit.health.spring.MyMath;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class HealthApplicationTests {
    //自动注入sysReportDao到测试环境里
    @Autowired
    SysReportDao sysReportDao;

    @Autowired
    //声明注入对象变量名zhangsan
    @Qualifier("zhangsan")
    Human human;

    @Autowired
    @Qualifier("myMath")
    MyMath myMath;
    @Test
    void contextLoads() {
    }


    @Test
    public void testGetAddSysReport(){
        System.out.println("test save");
        SysReport sysReport=new SysReport();
        sysReport.setId(1L);
        sysReport.setRemark("郭键航");
        sysReportDao.addSysReport(sysReport);
    }

    @Test
    public void testHuman(){
        human.print();
    }
    @Test
    public void testAOP(){
        myMath.add();
        myMath.susbtract();
    }
}
