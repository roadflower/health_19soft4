package edu.gdit.health;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication

@ImportResource(locations={"classpath:spring-common.xml"})
public class HealthApplication {

    public static void main(String[] args) {
        //test push
          //创建HealthApplication类对象app
        SpringApplication app=new SpringApplication(HealthApplication.class);
        //设置不使用banner
       // app.setBannerMode(Banner.Mode.OFF);
        //启动应用程序
        app.run(args);
    }

}
