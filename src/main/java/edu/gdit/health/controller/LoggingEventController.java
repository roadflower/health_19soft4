package edu.gdit.health.controller;

import edu.gdit.health.base.result.Results;
import edu.gdit.health.service.LoggingEventService;
import edu.gdit.health.service.impl.LoggingEventServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/logging")
public class LoggingEventController {
    @Autowired
    LoggingEventService loggingEventServiceImpl;

    @RequestMapping(value = "/list")
    @ResponseBody
    public Results list(int page,int limit){
        int offset=(page-1)*limit;
        return loggingEventServiceImpl.getLogging(offset,limit);
    }

}
