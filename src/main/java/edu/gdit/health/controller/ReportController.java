package edu.gdit.health.controller;

import com.alibaba.druid.support.logging.Log;
import com.alibaba.druid.support.logging.LogFactory;
import edu.gdit.health.base.result.ResponseCode;
import edu.gdit.health.base.result.Results;
import edu.gdit.health.model.SysDict;
import edu.gdit.health.model.SysReport;
import edu.gdit.health.service.SysReportService;
import edu.gdit.health.service.impl.SysReportServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.jws.WebParam;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//
@Controller
//定义该控制器的访问形式\report
@RequestMapping("/report")
public class ReportController {
   private static final Log log=LogFactory.getLog(ReportController.class);
    //自动注入service层对象sysReportServiceImpl，该对象由Spring容器创建并管理
    @Autowired
    SysReportService sysReportServiceImpl;


    //以/report/add的Get形式访问，则进入/templates/report/add.html页面
    @RequestMapping("/add")
    public String  add(Model model){
        //定义一个获取数据字典的对象dict
        Map<String, List<SysDict>>  dict=sysReportServiceImpl.getDictSet();
        //将dict返回到前端页面
        model.addAttribute("dict",dict);
        return  "/report/add";
    }

    //以/report/add的Post形式访问，则进入下面的方法，回到/report/add.html页面
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @ResponseBody
//@RequestBody主要用来接收前端传递给后端的json字符串中的数据的(请求体中的数据的)；GET方式无请求体，所以使用@RequestBody接收数据时，前端不能使用GET方式提交数据，而是用POST方式进行提交。
    public Results addPost(@RequestBody  SysReport sysReport){

        //调用service层的对象sysReportServiceImpl的addSysReport方法，添加一条数据
         return sysReportServiceImpl.addSysReport(sysReport);
    }

    @RequestMapping(value = "/list")
    @ResponseBody
//@RequestBody主要用来接收前端传递给后端的json字符串中的数据的(请求体中的数据的)；GET方式无请求体，所以使用@RequestBody接收数据时，前端不能使用GET方式提交数据，而是用POST方式进行提交。
    //SpringMVC框架自动获取同名参数赋值
    public Results list(int page,int limit){

        log.info("19软件4班同学打开了report/list");
        int offset=(page-1)*limit;
        //调用service层的对象sysReportServiceImpl的addSysReport方法，添加一条数据
        return sysReportServiceImpl.getReports(offset,limit);
    }

    //定义访问的url
    @RequestMapping("/getTemp")
    //定义返回json数据
    @ResponseBody
    public Results getTemp(){
        return  Results.success(ResponseCode.SUCCESS,sysReportServiceImpl.countTemp());
    }
}
