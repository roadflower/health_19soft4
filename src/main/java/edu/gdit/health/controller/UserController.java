package edu.gdit.health.controller;

import edu.gdit.health.base.result.Results;
import edu.gdit.health.model.SysDict;
import edu.gdit.health.model.SysUser;
import edu.gdit.health.service.SysReportService;
import edu.gdit.health.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
//定义该控制器的访问形式user
@RequestMapping("/user")
public class UserController {

    @Autowired
    SysUserService sysUserServiceImpl;

    @RequestMapping(value = "/list")
    @ResponseBody
//@RequestBody主要用来接收前端传递给后端的json字符串中的数据的(请求体中的数据的)；GET方式无请求体，所以使用@RequestBody接收数据时，前端不能使用GET方式提交数据，而是用POST方式进行提交。
    //SpringMVC框架自动获取同名参数赋值
    public Results list(int page, int limit){

        int offset=(page-1)*limit;
        //调用service层的对象sysReportServiceImpl的addSysReport方法，添加一条数据
        return sysUserServiceImpl.getUsers(offset,limit);
    }

    @GetMapping("/update")
    public String update(){
        return "/user/update";
    }


    @RequestMapping("/update")
    @ResponseBody
    public Results update(@RequestBody SysUser sysUser){
        return sysUserServiceImpl.update(sysUser);
    }

    @RequestMapping("/delete")
    @ResponseBody
    public Results delete(int id){
        return sysUserServiceImpl.delete(id);
    }

    
}
