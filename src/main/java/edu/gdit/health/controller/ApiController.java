package edu.gdit.health.controller;

import com.alibaba.druid.support.logging.Log;
import com.alibaba.druid.support.logging.LogFactory;
import edu.gdit.health.config.MD5;
import edu.gdit.health.model.SysUser;
import edu.gdit.health.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

//声明控制器组件
@Controller
//定义访问该组件的url形式,层级式
@RequestMapping("api")
public class ApiController {
    private static final Log log = LogFactory.getLog(ApiController.class);
    //定义方法的访问形式
    @RequestMapping("/test")
    public String test(){
        //转向到/resources/templates/test.html页面
        return "test";}

    @RequestMapping("/getPage")
    public ModelAndView getPage(ModelAndView mv,String pageName)
    {
        log.warn("19软件4班用户打开了新页面"+pageName);
        //如果pageName为空，则pageName= Index.html,否则不变
        pageName=null==pageName?"index":pageName;
        //设置转向视图的名称
        mv.setViewName(pageName);
        //设置数据
        mv.addObject("username","julia");
        mv.addObject("password","123456");
        //返回模型视图对象
        return mv;
    }

    @Autowired
    private SysUserService sysUserServiceImpl;



    @RequestMapping(value = {"/login"}, method = RequestMethod.GET)
    public String loginIndex() {
        return "login";
    }

    @RequestMapping(value = {"/login"}, method = RequestMethod.POST)
    public String login(@RequestParam(name = "username")String username, @RequestParam(name = "password")String password,
                        Model model, HttpServletRequest request) {
        SysUser user = sysUserServiceImpl.getPwd(username);
        String pwd = user.getPassword();
        String password1 = MD5.crypt(password);
        if (pwd.equals(password1)) {
            model.addAttribute("user", user);
            request.getSession().setAttribute("user", user);
            return "index";
        } else {
            return "login";
        }
    }
}
