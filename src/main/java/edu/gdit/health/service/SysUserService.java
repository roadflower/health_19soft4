package edu.gdit.health.service;

import edu.gdit.health.base.result.Results;
import edu.gdit.health.model.SysUser;

public interface SysUserService {
    Results getUsers(int offset,int limit);

    Results update(SysUser sysUser);

    Results delete(int id);

     SysUser getPwd(String username) ;

}
