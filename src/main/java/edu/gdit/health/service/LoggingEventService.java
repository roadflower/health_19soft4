package edu.gdit.health.service;

import edu.gdit.health.base.result.Results;

public interface LoggingEventService {
    Results getLogging(int offset,int limit);
}
