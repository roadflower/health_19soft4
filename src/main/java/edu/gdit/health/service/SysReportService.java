package edu.gdit.health.service;

import edu.gdit.health.base.result.Results;
import edu.gdit.health.model.SysDict;
import edu.gdit.health.model.SysReport;

import java.util.List;
import java.util.Map;

public interface SysReportService {
    Results addSysReport(SysReport sysReport);
    Map<String, List<SysDict>>  getDictSet();
    Results getReports(int offset,int limit);
    int count();
    //封装不同体温类型总人数到Results类的data属性，返回Results
    Results countTemp();
}
