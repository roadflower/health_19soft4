package edu.gdit.health.service.impl;

import edu.gdit.health.base.result.ResponseCode;
import edu.gdit.health.base.result.Results;
import edu.gdit.health.dao.SysReportDao;
import edu.gdit.health.model.SysDict;
import edu.gdit.health.model.SysReport;
import edu.gdit.health.model.SysReportTemp;
import edu.gdit.health.service.SysReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

//声明一个service对象，当项目启动时，Spring容器会创建一个名字叫sysReportServiceImpl的对象
@Service
public class SysReportServiceImpl implements SysReportService {

    //注入dao层对象sysReportDao,该对象由Spring容器创建并管理
    @Autowired
    SysReportDao sysReportDao;

    @Override
    public Results addSysReport(SysReport sysReport) {
       return Results.success(sysReportDao.addSysReport(sysReport));
    }

    @Override
    public Map<String, List<SysDict>> getDictSet() {
        Map<String,List<SysDict>> dict=new HashMap<>();
        //放置三对（key,value）
        dict.put("temperature",new ArrayList<SysDict>());
        dict.put("travel",new ArrayList<SysDict>());
        dict.put("physical_condition",new ArrayList<SysDict>());
        //为每对（key,value）的value赋值,第一个temperature
        dict.get("temperature").add(new SysDict("NORMAL","正常 37.3℃ 以下"));
        dict.get("temperature").add(new SysDict("LOW" , "低热 37.3-37.9℃"));
        dict.get("temperature").add(new SysDict("MIDDLE" , "中热 38-39℃"));
        dict.get("temperature").add(new SysDict("HIGH" , "高热 39℃以上"));
        //为每对（key,value）的value赋值,第二个travel
        dict.get("travel").add(new SysDict("1","有旅游"));
        dict.get("travel").add(new SysDict("0","没有旅游"));

        //为每对（key,value）的value赋值,physical_condition
        dict.get("physical_condition").add(new SysDict("travel" , "14天内曾居住或前往疫情高发地"));
        dict.get("physical_condition").add(new SysDict("contact" , "两周内有与确诊患者接触"));
        dict.get("physical_condition").add(new SysDict("normal" , "没有出现症状"));
        dict.get("physical_condition").add(new SysDict("rheum" , "感冒样症状：乏力、精神差、咳嗽、发烧、肌肉痛、头痛"));
        dict.get("physical_condition").add(new SysDict("polypnea" , "喘憋、呼吸急促"));
        dict.get("physical_condition").add(new SysDict("vomiting" , "恶心呕吐、腹泻"));
        dict.get("physical_condition").add(new SysDict("flustered" , "心慌、胸闷"));
        dict.get("physical_condition").add(new SysDict("conjunctivitis" , "结膜炎（红眼病样表现：眼睛涩、红、分泌物）"));
        return dict;
    }

    @Override
    public Results getReports(int offset,int limit) {
        //获取dao返回的记录总行数
        int count=sysReportDao.count();
        return Results.success(count,sysReportDao.getAllReports(offset,limit));
    }

    @Override
    public int count() {
        return sysReportDao.count();
    }

    @Override
    public Results countTemp() {
        //将sys_report表里不同体温类型的总人数存到data列表对象
        List data=new ArrayList();
        data.add(sysReportDao.countTemp("NORMAL"));
        data.add(sysReportDao.countTemp("lOW"));
        data.add(sysReportDao.countTemp("MIDDLE"));
        data.add(sysReportDao.countTemp("HIGH"));
        //封装data数据，相应成功信息，并且返回
        return Results.success(ResponseCode.SUCCESS,data);
    }


}
