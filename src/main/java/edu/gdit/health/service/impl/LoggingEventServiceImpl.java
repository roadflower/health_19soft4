package edu.gdit.health.service.impl;

import edu.gdit.health.base.result.Results;
import edu.gdit.health.dao.LoggingEventDao;
import edu.gdit.health.service.LoggingEventService;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoggingEventServiceImpl implements LoggingEventService {
    @Autowired
    LoggingEventDao loggingEventDao;
    @Override
    public Results getLogging(int offset, int limit) {
        int count=loggingEventDao.count();
        return Results.success(count,loggingEventDao.getLogging(offset,limit));
    }
}
