package edu.gdit.health.service.impl;

import edu.gdit.health.base.result.Results;
import edu.gdit.health.dao.SysUserDao;
import edu.gdit.health.model.SysUser;
import edu.gdit.health.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysUserServiceImpl implements SysUserService {
    @Autowired
    SysUserDao sysUserDao;

    @Override
    public Results getUsers(int offset, int limit) {
        //获取dao返回的记录总行数
        int count=sysUserDao.count();
        return Results.success(count,sysUserDao.getAllUsers(offset,limit));
    }

    @Override
    public Results update(SysUser sysUser) {
        return Results.success(sysUserDao.update(sysUser));
    }

    @Override
    public Results delete(int id) {
        return Results.success(sysUserDao.delete(id));
    }

    @Override
    public SysUser getPwd(String username) {
        return sysUserDao.getPwd(username);
    }
}
