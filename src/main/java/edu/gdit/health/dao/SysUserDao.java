package edu.gdit.health.dao;

import edu.gdit.health.model.SysReport;
import edu.gdit.health.model.SysUser;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface SysUserDao {
    //获取Sys_user表里所有的数据，返回List对象
    @Select("select *  from  sys_user limit #{offset},#{limit}")
    List<SysUser> getAllUsers(int offset, int limit);

    //计算sys_user表的记录总行数
    @Select("select count(*) from sys_user")
    int count();

    @Update("update sys_user set username=#{username},password=#{password},nickname=#{nickname},email=#{email} where id=#{id}")
    int update(SysUser sysUser);

    @Delete("delete from sys_user where id=#{id}")
    int delete(int id);
    @Select("select username,password from sys_user where username=#{username}")
    public SysUser getPwd(String username);
}
