package edu.gdit.health.dao;

import edu.gdit.health.base.result.ResponseCode;
import edu.gdit.health.model.SysReport;
import edu.gdit.health.model.SysReportCount;
import edu.gdit.health.model.SysReportTemp;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

//@Mapper注解的意思：Spring容器会根据接口SysReportDao实例化一个dao对象sysReportDao
@Mapper
public interface SysReportDao {
    //获取Sys_report表里所有的数据，返回List对象
    @Select("select *  from  sys_report limit #{offset},#{limit}")
    List<SysReport> getAllReports(int offset,int limit);
    //增加一条SysReport的记录
    @Insert("insert into sys_report(createTime,user_id,remark,temperature,travel,travel_description,physical_condition) "+
            "values(now(),#{userId},#{remark},#{temperature},#{travel},#{travelDescription},#{physicalCondition})")
    int addSysReport(SysReport sysReport);
    //计算sys_report表的记录总行数
    @Select("select count(*) from sys_report")
    int count();

    //计算sys_report表的不同体温类型的总行数
    @Select("select count(*)   from sys_report where temperature=#{temp}")
    int countTemp(String temp);

}
