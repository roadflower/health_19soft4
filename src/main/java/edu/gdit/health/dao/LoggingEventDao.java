package edu.gdit.health.dao;


import edu.gdit.health.model.LoggingEvent;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;


@Mapper
public interface LoggingEventDao {
    @Select("select * from logging_event limit #{offset},#{limit}")
    List<LoggingEvent> getLogging(int offset,int limit);
    //计算表logging_event的记录总行数
    @Select("select count(*) from logging_event")
    int count();
}
