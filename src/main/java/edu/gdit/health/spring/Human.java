package edu.gdit.health.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("zhangsan")
public class Human {
    @Value("张三")
    private String name;
    //自动注入
    @Autowired
    //声明注入对象变量名
    @Qualifier("hongqi")
    private Car car;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", car=" + car +
                '}';
    }
    public void print(){
        System.out.println(toString());
    }
}
