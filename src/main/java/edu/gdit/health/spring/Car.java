package edu.gdit.health.spring;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
//@Component实例化一个对象，组件/部件，@Controller,@Service,@Mapper,@Repository细分类
@Component("littlebear")

public class Car {
    //@Value适用于Java的基本数据类型和String
    @Value("Nissan")
    private String type;
    @Value("200")
    private int maxSpeed;

    public Car() {
    }

    public Car(String type, int maxSpeed) {
        this.type = type;
        this.maxSpeed = maxSpeed;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Override
    public String toString() {
        return "Car{" +
                "type='" + type + '\'' +
                ", maxSpeed=" + maxSpeed +
                '}';
    }
    public void print(){
        System.out.println(toString());
    }
}
