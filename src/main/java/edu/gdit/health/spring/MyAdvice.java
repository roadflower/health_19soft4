package edu.gdit.health.spring;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
//声明增强
@Aspect
public class MyAdvice {
    //说明before方法前置方法，方法里的参数声明切点,*表示切在所有方法上，..表示任意参数个数
    @Before("execution(* edu.gdit.health.spring.MyMath.add(..))")
    public void before(){
        System.out.println("-------I am before advice----------");
    }
    //after后置方法，方法里的参数声明切点,*表示切在所有方法上，..表示任意参数个数
    @After("execution(* edu.gdit.health.spring.MyMath.s*(..))")
    public void after(){
        System.out.println("-------I am after advice----------");
    }
}
