package edu.gdit.health.spring;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("myMath")
public class MyMath {
    @Value("10")
    private int num1;
    @Value("5")
    private  int num2;
    private  float result;

    public int getNum1() {
        return num1;
    }

    public void setNum1(int num1) {
        this.num1 = num1;
    }

    public int getNum2() {
        return num2;
    }

    public void setNum2(int num2) {
        this.num2 = num2;
    }

    public float getResult() {
        return result;
    }

    public void add(){
        result=num1+num2;
        System.out.println(result);
    }
    public void  susbtract(){
        result=num1-num2;
        System.out.println(result);
    }
}
